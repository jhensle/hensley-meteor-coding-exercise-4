// A response choice that a player can give as a result of a non playable character request

PlayerResponse = new Mongo.Collection('player_response');

PlayerResponse.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  objectCode:{
    type:String
  },
  npCharacterRequestCodes : {
    type:[String],
    optional: true
  },
  isCorrectAnswer : {
    type:Boolean,
    optional: true
  }


}));

/*
 * Add query methods like this:
 *  PlayerResponse.findPublic = function () {
 *    return PlayerResponse.find({is_public: true});
 *  }
 */